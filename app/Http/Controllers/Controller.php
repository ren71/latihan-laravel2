<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function table()
    {
        return view('halaman.table');
    }
    public function datatable (){
        return view ('halaman.data-table');
    }
    public function home (){
        return view ('halaman.home');
    }
}
