<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
route::get('/','Controller@home');
route::get('/table','Controller@table');
route::get('/data-table','Controller@datatable');

//CRUD cast

//Form input data create cast
route::get('/cast/create', 'CastController@create');

//untuk  menyiimpan data ke database
route::post('/cast', 'CastController@store');

//read
//menambiltan data 
route::get('/cast', 'CastController@index');

route::get('/cast/{cast_id}', 'CastController@show');

//edit data
route::get('/cast/{cast_id}/edit', 'CastController@edit');
//update
route::put('/cast/{cast_id}','CastController@update');

//delete
route::delete('/cast/{cast_id}','CastController@destroy');